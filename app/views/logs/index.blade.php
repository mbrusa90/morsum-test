@extends('layouts.master')

@section('content')
	<h1>Log history</h1>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Email</th>
					<th>Date</th>
				</tr>
			</thead>

			<tbody>
				@foreach ($logs as $log)
				<tr>
					<td>{{ $log->user->email }}</td>
					<td>{{ date('Y/m/d H:i:s', strtotime($log->created_at)) }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@stop