<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Morsum</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
        
        @if (Auth::check())
        <li><a href="{{ URL::to('users') }}">Users</a></li>
        <li><a href="{{ URL::to('logs') }}">Log history</a></li>
        @endif


      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        @if (!Auth::check())
        <li><a href="{{ URL::to('users/login') }}">Login</a></li>
        <li><a href="{{ URL::to('users/signup') }}">Signup</a></li>
        @endif
        @if (Auth::check())
        <li><a href="{{ URL::to('users/logout') }}">Logout</a></li>
        @endif
      </ul>
      
    </div>
  </div>
</nav>