@extends('layouts.master')

@section('content')
<div class=" col-md-6 col-md-push-3">

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Login</h3>
		</div>
	  <div class="panel-body">
	  	{{ Form::open() }}
		@if($errors->has())
			@foreach ($errors->all() as $error)
			  	<div class="alert alert-danger" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					<span class="sr-only">Error:</span>
					{{ $error }}
				</div>
			@endforeach
		@endif
		
		<div class="form-group">
			{{ Form::label('email', 'Email Address') }}
			{{ Form::email('email', Input::old('email'), array('placeholder' => 'Email', 'class' => 'form-control')) }}
		</div>

		<div class="form-group">
			{{ Form::label('password', 'Password') }}
		    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
		</div>

		<p>{{ Form::submit('Login', array('class' => 'btn btn-default')) }}</p>
		{{ Form::close() }}
	  </div>
	</div>
</div>
@stop