@extends('layouts.master')

@section('content')
	<h1>Users</h1>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Full Name</th>
					<th>Email</th>
					<th>Phone</th>
				</tr>
			</thead>

			<tbody>
				@foreach ($users as $user)
				<tr>
					<td>{{ $user->name }} {{ $user->last_name }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->phone }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@stop