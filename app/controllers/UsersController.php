<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();

		return View::make('users.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), User::$rules);

		if ($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}

		User::create($data);

		return Redirect::route('users.index');
	}

	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);

		return View::make('users.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);

		return View::make('users.edit', compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$user = User::findOrFail($id);

		$validator = Validator::make($data = Input::all(), User::$rules);

		if ($validator->fails()){
		    return Redirect::back()->withErrors($validator)->withInput();
		}

		$user->update($data);

		return Redirect::route('users.index');
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::destroy($id);

		return Redirect::route('users.index');
	}


	public function showLogin()
	{
		return View::make('users.login');
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::to('users/login');
	}

	public function postLogin()
	{
		$rules = array(
			'email'    => 'required|email',
			'password' => 'required|alphaNum|min:3'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			return Redirect::to('users/login')
		        ->withErrors($validator)
		        ->withInput(Input::except('password')); 
		} else {
		    $userdata = array(
		        'email'     => Input::get('email'),
		        'password'  => Input::get('password')
		    );
			if (Auth::attempt($userdata)) {
				$log = new Userlog();
				$log->user_id = Auth::user()->id;
				$log->save();
				return Redirect::to('users');
		    } else {
		        return Redirect::to('users/login');
		    }
		}
	}

	public function showSignup()
	{
		return View::make('users.signup');
	}

	public function postSignup()
	{
		$input = Input::all();
		$rules = array(
			'name' => 'required', 
			'last_name' => 'required', 
			'email' => 'required|unique:users|email', 
			'password' => 'required|alphaNum|min:3'
		);
		$v = Validator::make($input, $rules);
		if($v->passes()){
			$password = Hash::make($input['password']);
			$user = new User();
			$user->name = $input['name'];
			$user->last_name = $input['last_name'];
			$user->phone = $input['phone'];
			$user->email = $input['email'];
			$user->password = $password;
			$user->save();

			$userdata = array(
		        'email'     => $input['email'],
		        'password'  => $input['password']
		    );
			if (Auth::attempt($userdata)) {
				$log = new Userlog();
				$log->user_id = Auth::user()->id;
				$log->save();
				return Redirect::to('users');
		    } else {
		        return Redirect::to('users/login');
		    }
		} else {
			return Redirect::to('users/signup')->withInput()->withErrors($v);
		}
	}

}