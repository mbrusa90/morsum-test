<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return Redirect::to('users/login');
});

Route::get('users/login', array('uses' => 'UsersController@showLogin'));
Route::post('users/login', array('uses' => 'UsersController@postLogin'));

Route::get('users/logout', array('uses' => 'UsersController@logout'));

Route::get('users/signup', array('uses' => 'UsersController@showSignup'));
Route::post('users/signup', array('uses' => 'UsersController@postSignup'));

Route::resource('users', 'UsersController');
Route::resource('logs', 'LogsController');
